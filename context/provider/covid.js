import React from "react";
import axios from "axios";

export const CovidContext = React.createContext({});

const CovidProvider = ({ children }) => {
  const [info, setInfo] = React.useState(null);

  const getGlobalInfo = async () => {
    const res = await axios.get("https://coronavirus-19-api.herokuapp.com/all");
    setInfo(res.data);
  };

  const getAllCountries = async () => {
    const res = await axios.get(
      "https://coronavirus-19-api.herokuapp.com/countries"
    );
    return res.data;
  };

  const getInfoByCountryName = async name => {
    const res = await axios.get(
      `https://coronavirus-19-api.herokuapp.com/countries/${name}`
    );
    return res.data;
  };

  const getInfoByCountryDates = async name => {
    const newSearch = Array.from(name).map((symb, index) => {
      if (index === 0) {
        return symb.toLowerCase();
      } else {
        return symb;
      }
    }).join('');

    const res = await axios.get(
        `https://corona.lmao.ninja/historical/${newSearch}`
    );
    return res.data;
  };

  React.useEffect(() => {
    getGlobalInfo();
  }, []);

  const values = {
    info,
    getAllCountries,
    getInfoByCountryName,
    getInfoByCountryDates
  };

  return (
    <CovidContext.Provider value={values}>{children}</CovidContext.Provider>
  );
};

export default CovidProvider;
