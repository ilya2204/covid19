import * as React from "react";
import Header from "../components/Header";
import MobileHeader from "../components/MobileHeader";
import Footer from "../components/Footer";

const MainLayout = ({ children }) => {
  return (
    <>
      <Header />
      <MobileHeader />
      {children}
      <Footer />
    </>
  );
};

export default MainLayout;
