import * as React from "react";
import styled from "styled-components";
import Router from "next/router";

const Wrapper = styled.div`
  width: 100%;
  display: flex;
  background-color: black;
  color: white;
  padding: 15px 0px;
  justify-content: space-around;

  @media (max-width: 767px) {
    display: none;
  }

  @media (max-width: 425px) {
    flex-direction: column;
  }
`;

const MenuTab = styled.div`
  cursor: pointer;
`;

const MenuWrapper = styled.div`
  display: flex;
  width: 300px;
  align-items: center;
  justify-content: space-between;

  @media (max-width: 425px) {
    width: auto;
    padding: 9px 20px;

    ${MenuTab} {
      margin-right: 5px;
    }
  }
`;

const Logo = styled.img`
  width: 30px;
  height: 30px;
`;

const LogoSection = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  img {
    margin-right: 10px;
  }
`;

const Header = () => {
  return (
    <Wrapper>
      <LogoSection>
        <Logo src="/bacterium.svg" /> CoV-19
      </LogoSection>
      <MenuWrapper>
        <MenuTab onClick={() => Router.push("/")}>Global Info</MenuTab>
        <MenuTab onClick={() => Router.push("/countries")}>Countries</MenuTab>
        <MenuTab onClick={() => Router.push("/about")}>About</MenuTab>
      </MenuWrapper>
    </Wrapper>
  );
};

export default Header;
