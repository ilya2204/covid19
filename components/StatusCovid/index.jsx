import * as React from "react";
import styled from "styled-components";
import { CovidContext } from "context/provider/covid";
import dynamic from "next/dynamic";
const Chart = dynamic(import("./Chart"));

const Wrapper = styled.div`
  padding: 10px 25px;
  display: flex;
  flex-direction: row;
  justify-content: space-around;
  flex-wrap: wrap;
`;

const CardWrapper = styled.div`
  width: 325px;
  height: 160px;
  border: 1px solid black;
  margin-top: 15px;
  padding: 10px;
  border-radius: 30px;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

const Title = styled.div`
  font-size: 33px;
  color: ${({ color }) => color || "orange"};
  font-weight: bold;
`;

const Count = styled.div`
  font-size: 55px;
`;

const MainWrapper = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
`;

const StatusCovid = () => {
  const { info } = React.useContext(CovidContext);

  const percentageDie = !!info
    ? ((info.deaths * 100) / info.cases).toFixed(2) + "%"
    : "N/A";

  return (
    <MainWrapper>
      <Wrapper>
        <CardWrapper>
          <Title>Covid cases</Title>
          <Count>{info?.cases}</Count>
        </CardWrapper>
        <CardWrapper>
          <Title color="green">Recovered</Title>
          <Count>{info?.recovered}</Count>
        </CardWrapper>
        <CardWrapper>
          <Title color="red">Deaths</Title>
          <Count>{info?.deaths}</Count>
        </CardWrapper>
        <CardWrapper>
          <Title color="#AF0000">% of the deaths</Title>
          <Count>{percentageDie} </Count>
        </CardWrapper>
      </Wrapper>
      <Chart info={info} />
    </MainWrapper>
  );
};

export default StatusCovid;
