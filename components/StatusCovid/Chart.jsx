import * as React from "react";
import { ResponsiveContainer, PieChart, Pie, Tooltip } from "recharts";
import styled from "styled-components";

const WrapperChart = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
  font-size: 11px;
  height: 300px;

  .recharts-responsive-container {
    max-width: 300px;
  }
`;

const Chart = ({ info }) => {
  const data = [
    {
      name: "Cases",
      value: info?.cases || 10,
      fill: "orange"
    },
    {
      name: "Recovered",
      value: info?.recovered || 10,
      fill: "green"
    },
    {
      name: "Deaths",
      value: info?.deaths,
      fill: "red"
    }
  ];

  return (
    <WrapperChart>
      <ResponsiveContainer>
        <PieChart>
          <Pie
            dataKey="value"
            isAnimationActive={false}
            data={data}
            cx={135}
            cy={150}
            outerRadius={100}
            fill="#8884d8"
            label
          />
          <Tooltip />
        </PieChart>
      </ResponsiveContainer>
    </WrapperChart>
  );
};

export default Chart;
