import * as React from "react";
import styled from "styled-components";

const Wrapper = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  font-size: 18px;
  height: 20px;
`;

const Footer = () => {
  return <Wrapper></Wrapper>;
};

export default Footer;
