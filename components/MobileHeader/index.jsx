import * as React from "react";
import styled from "styled-components";
import Router from "next/router";

const Wrapper = styled.div`
  width: 100%;
  display: none;
  background-color: black;
  color: white;
  padding: 15px 20px;
  justify-content: space-between;

  @media (max-width: 767px) {
    display: flex;
    align-items: center;
  }
`;

const MenuTab = styled.div`
  display: block;
  padding: 0.75em 15px;
  line-height: 1em;
  font-size: 1em;
  color: #fff;
  text-decoration: none;
  border: 1px solid #383838;
  border-radius: 20px;
`;

const MenuWrapper = styled.div`
  display: flex;
  color: white;
  width: 300px;
  align-items: center;
  justify-content: space-between;
  flex-direction: column;

  @media (max-width: 425px) {
    width: 100%;
    padding: 25px 20px;
    height: 100%;
    display: flex;
    justify-content: center;

    ${MenuTab} {
      width: 100%;
      margin-bottom: 10px;
      margin-right: 5px;
    }
  }
`;

const Logo = styled.img`
  width: 30px;
  height: 30px;
`;

const LogoSection = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  img {
    margin-right: 10px;
  }
`;

export const MenuIcon = styled.img`
  width: 23px;
  height: 23px;
  cursor: pointer;
`;

const Menu = styled.div`
  padding: 10px 20px;
  width: 100vw;
  color: white;
  min-height: 100%;
  position: absolute;
  background-color: black;
  z-index: 99999;
  top: 0;
  height: 100vh;
  display: ${({ isOpen }) => (isOpen ? "block" : "none")};
`;

const CloseImg = styled.img`
  width: 20px;
  height: 20px;
  position: absolute;
  top: 10px;
  right: 20px;
`;

const MobileHeader = () => {
  const [isOpen, setOpen] = React.useState(false);
  React.useEffect(() => {
    if (isOpen) {
      document.body.style.overflow = "hidden";
    } else {
      document.body.style.overflow = "unset";
    }
  }, [isOpen]);

  const relink = link => {
    setOpen(false);
    Router.push(`/${link}`);
  };

  return (
    <>
      <Wrapper>
        <LogoSection>
          <Logo src="/bacterium.svg" /> CoV-19
        </LogoSection>
        <MenuIcon src={"/menu-icon.svg"} onClick={() => setOpen(true)} />
      </Wrapper>
      <Menu isOpen={isOpen}>
        <MenuWrapper>
          <MenuTab onClick={() => relink("")}>Global Info</MenuTab>
          <MenuTab onClick={() => relink("countries")}>Countries</MenuTab>
          <MenuTab onClick={() => relink("about")}>About</MenuTab>
        </MenuWrapper>
        <CloseImg src="/close.svg" onClick={() => setOpen(false)} />
      </Menu>
    </>
  );
};

export default MobileHeader;
