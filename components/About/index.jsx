import * as React from "react";
import styled from "styled-components";

const Wrapper = styled.div`
  padding: 10px 25px;
  display: flex;
  font-family: Muil;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

const LinkMe = styled.a`
  color: black;
  text-transform: none;
  text-decoration: none;
  cursor: pointer;
  font-weight: bold;
  margin-top: 20px;
  display: flex;
  justify-content: space-between;
  align-items: center;
  width: 110px;
`;

const Image = styled.img`
  width: 20px;
`;

const AboutComponent = () => {
  return (
    <Wrapper>
      You can find me in:
      <LinkMe href="tg://resolve?domain=ilya2204">
        - Telegram <Image src={"/tel.svg"} />
      </LinkMe>
      <LinkMe href="mailto:ilyaburmaka@gmail.com">
        - E-mail <Image src={"/mail.svg"} />
      </LinkMe>
      <LinkMe
        href="https://www.linkedin.com/in/ilya-burmaka-0b1079161/"
        target="_blank"
      >
        - LinkedIn <Image src={"linkedin.svg"} />
      </LinkMe>
    </Wrapper>
  );
};

export default AboutComponent;
