import * as React from "react";
import styled from "styled-components";
import { CovidContext } from "context/provider/covid";
import Router from "next/router";
import Flag from "react-world-flags";
import arrCountries from "constants/countries";

const Wrapper = styled.div`
  padding: 10px 25px;
  max-width: 1050px;
  margin: 0 auto;
  display: grid;
  grid-gap: 1rem;
  grid-template-columns: repeat(auto-fit, minmax(200px, 1fr));
`;

const Name = styled.div`
  margin-left: 10px;
`;

const DetailCardCountry = styled.div`
  width: 100%;
  cursor: pointer;
  font-size: 16px;
  height: 100px;
  flex: 1 1 auto;
  display: flex;
  justify-content: center;
  border-radius: 5px;
  align-items: center;
  flex-direction: row;
  background-color: whitesmoke;
  transition: 500ms;

  :hover {
    box-shadow: 1px 1px 8px 4px rgba(0, 0, 0, 0.1);

    ${Name} {
      transition: 500ms;
      text-shadow: 0px 4px 20px rgba(111, 111, 117, 0.65);
    }
  }
`;

const FoundNothing = styled.div`
  width: 100%;
  text-align: center;
  font-family: Muli;
  font-size: 25px;
  margin: 10px;
`;

const StyledInput = styled.input`
  border-radius: 40px;
  border: 2px solid black;
  height: 30px;
  padding: 2px 8px;
  width: 100%;
  max-width: 270px;

  :focus {
    outline: none;
  }
`;

const SearchWrapper = styled.div`
  margin-top: 15px;
  margin-bottom: 8px;
  display: flex;
  justify-content: center;
`;

const LoadMore = styled.div`
  display: flex;
  cursor: pointer;
  padding: 8px;
  justify-content: center;
`;
const Countries = () => {
  const [searchValue, setSearchValue] = React.useState("");
  const [countries, setCountries] = React.useState(null);
  const [valueRes, setRes] = React.useState(null);
  const [count, setCount] = React.useState(1);
  const { getAllCountries } = React.useContext(CovidContext);
  let maxCount = countries?.length / 24;
  let maxCountBySearch = valueRes?.length / 24;

  React.useEffect(() => {
    getAllCountries().then(async data => {
      const country = data.map(item => item.country).sort();
      await setCountries(country);
      setRes(country);
    });
  }, []);

  React.useEffect(() => {
    if (searchValue.length !== 0) {
      setCount(1);
      const newRes = countries.filter(item => {
        if (searchValue.length === 1) {
          return (
            item.includes(searchValue.toUpperCase()) ||
            item.includes(searchValue.toLowerCase())
          );
        } else {
          const newSearch = Array.from(searchValue).map((symb, index) => {
            if (index === 0) {
              return symb.toUpperCase();
            } else {
              return symb;
            }
          });

          return item.includes(newSearch.join(""));
        }
      });
      setRes(newRes);
    } else {
      setRes(countries);
    }
  }, [searchValue]);

  const onLoadMore = () => {
    if (maxCount > count) {
      setCount(count + 1);
    }
  };

  return (
    <>
      <SearchWrapper>
        <StyledInput
          placeholder={"Country..."}
          value={searchValue}
          onChange={e => setSearchValue(e.target.value)}
        />
      </SearchWrapper>
      <Wrapper>
        {!!valueRes &&
          valueRes.slice(0, 24 * count).map(name => (
            <DetailCardCountry
              key={name + "country"}
              onClick={() => Router.push(`/details?name=${name}`)}
            >
              <Flag
                code={
                  arrCountries.find(item => item.name === name)?.code || "UA"
                }
                height="16"
              />
              <Name>{name}</Name>
            </DetailCardCountry>
          ))}
        {!!valueRes && valueRes.length === 0 && (
          <FoundNothing>N/A by you filter</FoundNothing>
        )}
      </Wrapper>
      {!searchValue && maxCount > count && (
        <LoadMore onClick={onLoadMore}>Load More</LoadMore>
      )}
      {!!searchValue && valueRes.length > 24 && maxCountBySearch > count && (
        <LoadMore onClick={onLoadMore}>Load More</LoadMore>
      )}
    </>
  );
};

export default Countries;
