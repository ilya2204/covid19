import * as React from "react";
import styled from "styled-components";
import { CovidContext } from "context/provider/covid";
import { useRouter } from "next/router";
import ChartDetails from "./Chart";
import Head from "next/head";

const Wrapper = styled.div`
  padding: 10px 25px;
  display: flex;
  flex-direction: column;
  font-family: Muli;
  align-items: center;
  justify-content: space-between;
  flex-wrap: wrap;
  margin-bottom: 50px;

  @media (max-width: 425px) {
    padding: 4px;
  }
`;

const Name = styled.div`
  font-size: 35px;
  text-align: center;
  margin-bottom: 50px;
  margin-top: 10px;
`;

const Title = styled.div`
  font-size: 20px;
  font-weight: bold;
  @media (max-width: 425px) {
    font-size: 17px;
  }
`;

const Count = styled.div`
  font-size: 18px;
  font-weight: 500;
  @media (max-width: 425px) {
    font-size: 15px;
  }
`;

const WrapperStatic = styled.div`
  display: flex;
  justify-content: space-between;
`;

const InnerWrapper = styled.div`
  padding: 10px;
  width: 350px;
  box-shadow: 1px 1px 8px 4px rgba(0, 0, 0, 0.1);

  @media (max-width: 425px) {
    width: 300px;
  }
`;

const DetailsCountry = () => {
  const { getInfoByCountryName } = React.useContext(
    CovidContext
  );

  const [info, setInfo] = React.useState({});
  const route = useRouter();

  React.useEffect(() => {
    getInfoByCountryName(route.query.name).then(data => {
      setInfo({ ...data });
    });
  }, []);

  const percentageDie = !!info
    ? ((info.deaths * 100) / info.cases).toFixed(2) + "%"
    : "0";

  return (
    <Wrapper>
      <Head>
        <title>Covid-19 - {info?.country}</title>
      </Head>
      <Name>{info?.country}</Name>
      <InnerWrapper>
        <WrapperStatic>
          <Title>Confirmed Cases</Title>
          <Count>{info?.cases}</Count>
        </WrapperStatic>
        <WrapperStatic>
          <Title>Recovered</Title>
          <Count>{info?.recovered}</Count>
        </WrapperStatic>
        <WrapperStatic>
          <Title>Today Cases</Title>
          <Count>{info?.todayCases}</Count>
        </WrapperStatic>
        <WrapperStatic>
          <Title>Deaths</Title>
          <Count>{info?.deaths}</Count>
        </WrapperStatic>
        <WrapperStatic>
          <Title>Active</Title>
          <Count>{info?.active}</Count>
        </WrapperStatic>
        <WrapperStatic>
          <Title>Critical</Title>
          <Count>{info?.critical}</Count>
        </WrapperStatic>
        <WrapperStatic>
          <Title>% of the deaths</Title>
          <Count>{percentageDie}</Count>
        </WrapperStatic>
      </InnerWrapper>
      {info && <ChartDetails info={info} />}
    </Wrapper>
  );
};

export default DetailsCountry;
