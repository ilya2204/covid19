import * as React from "react";
import { PieChart, Pie, ResponsiveContainer, Legend } from "recharts";
import styled from "styled-components";

const BarWrapper = styled.div`
  width: 100%;
  margin-top: 60px;
  max-width: 400px;
  font-size: 12px;
  height: 330px;
`;

const style = {
  top: -45,
  left: 10,
  lineHeight: "19px"
};

const ChartDetails = ({ info }) => {
  const data = [
    {
      name: "Today",
      value: info?.todayCases,
      fill: "pink"
    },
    {
      name: "Deaths",
      value: info?.deaths,
      fill: "red"
    },
    {
      name: "Cases",
      value: info?.cases,
      fill: "orange"
    },
    {
      name: "Recovered",
      value: info?.recovered,
      fill: "green"
    }
  ];

  return (
    <BarWrapper>
      <ResponsiveContainer>
        <PieChart width={730} height={280}>
          <Pie
            data={data}
            dataKey="value"
            nameKey="name"
            cx="50%"
            cy="50%"
            minAngle={8}
            innerRadius={40}
            outerRadius={80}
            label
          />
          <Legend
            iconSize={11}
            width={110}
            height={140}
            layout="vertical"
            verticalAlign="middle"
            wrapperStyle={style}
          />
        </PieChart>
      </ResponsiveContainer>
    </BarWrapper>
  );
};

export default ChartDetails;
