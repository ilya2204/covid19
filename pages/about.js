import React from "react";
import MainLayout from "../layouts/MainLayout";
import AboutComponent from "../components/About";
import Head from "next/head";

const CountriesPage = () => (
  <MainLayout>
      <Head>
          <title>Covid-19 - About</title>
      </Head>
    <AboutComponent />
  </MainLayout>
);

export default CountriesPage;
