import React from "react";
import MainLayout from "../layouts/MainLayout";
import dynamic from "next/dynamic";
const StatusCovid = dynamic(import("components/StatusCovid"));

const Index = () => (
  <MainLayout>
    <StatusCovid />
  </MainLayout>
);

export default Index;
