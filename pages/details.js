import React from "react";
import MainLayout from "../layouts/MainLayout";
import dynamic from "next/dynamic";

const DetailsCountry = dynamic(import("components/CountryDetails"));

const CountryPage = () => (
  <MainLayout>
    <DetailsCountry />
  </MainLayout>
);

export default CountryPage;
