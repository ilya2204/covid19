import React from "react";
import MainLayout from "../layouts/MainLayout";
import Countries from "../components/Countries";
import Head from "next/head";

const CountriesPage = () => (
  <MainLayout>
      <Head>
          <title>Covid-19 - Countries</title>
      </Head>
    <Countries />
  </MainLayout>
);

export default CountriesPage;
